USE classic_models;

--1
SELECT customerName
FROM customers
WHERE country = "Philippines";

--2
SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName = "La Rochelle Gifts";

--3
SELECT productName, MSRP
FROM products
WHERE productName = "The Titanic";

--4
SELECT firstName, lastName
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

--5
SELECT customerName
FROM customers
WHERE state IS NULL;

--6
SELECT firstName, lastName, email
FROM employees
WHERE lastName = "Patterson"
AND firstName = "Steve";

--7
SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA"
AND creditLimit > 3000;

--8
SELECT customerName
FROM customers
WHERE customerName NOT LIKE "%a%";

--9
SELECT customerNumber
FROM orders
WHERE comments LIKE "%DHL%";

--10
SELECT productline
FROM productlines
WHERE textDescription LIKE "%state of the art%";

--11
SELECT DISTINCT country
FROM customers;

--12
SELECT DISTINCT status
FROM orders;

--13
SELECT customerName, country
FROM customers
WHERE country = "USA"
OR country = "France"
OR country = "Canada";

--14
SELECT firstName, lastName, city
FROM employees JOIN offices
ON employees.officeCode = offices.officeCode
WHERE offices.city = "Tokyo";

--15
SELECT customerName
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.firstName = "Leslie"
AND employees.lastName = "Thompson";

--16
SELECT productName, customerName
FROM products JOIN orderdetails
ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = "Baane Mini Imports";

--17
SELECT firstName, lastName, customerName, offices.country
FROM employees JOIN customers
ON employees.employeeNumber = customers.salesRepEmployeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

--18
SELECT e.lastName, e.firstName
FROM employees e, employees m
WHERE e.reportsTo = m.employeeNumber
AND m.lastName = "Bow"
AND m.firstName = "Anthony";

--19
SELECT productName, MSRP
FROM products
ORDER BY MSRP DESC
LIMIT 1;

--20
SELECT COUNT(*) AS "Number of Customers in UK"
FROM customers
WHERE country = "UK";

--21
SELECT productline, COUNT(*)
FROM products
GROUP BY productLine;

--22
SELECT firstName, lastName, COUNT(*)
FROM employees JOIN customers
ON employees.employeeNumber = customers.salesRepEmployeeNumber
GROUP BY firstName;

--23
SELECT productName, quantityInStock
FROM products
WHERE productLine = "Planes"
AND quantityInStock < 1000;
